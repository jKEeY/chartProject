import { Line } from 'vue-chartjs'

export default {
	name: 'LineChart',
	extends: Line,
	props: {
		labels: {
			type: Array,
			required: true
		},
		dataPrice: {
			type: Array,
			required: true
		}
	},
	mounted() {
		this.renderChart({
			labels: this.labels,
			datasets: [{
				label: 'АИ92',
				backgroundColor: '#ff9baa',
				data: this.dataPrice
			}]
		})
	}
}