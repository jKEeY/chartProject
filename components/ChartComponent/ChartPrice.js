import { Bar } from 'vue-chartjs'

export default {
	name: 'ChartPrice',
	extends: Bar,
	props: {
		labels: {
			type: Array,
			required: true
		},
		dataPrice: {
			type: Array,
			required: true
		}
	},
	mounted() {
		this.renderChart({
			labels: this.labels,
			datasets: [{
				label: 'Цены на сегодня: 19.08.2018',
				backgroundColor: ['#ff9baa', '#66cdaa', '#a0d6b4'],
				data: this.dataPrice
			}]
		})
	}
}